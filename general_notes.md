Install the Digilent Cable Drivers.
===================================

*This procedure has been tested with the ISE 14.4*

Download the `Digilent Plugins for Xilinx Tools`.

    http://store.digilentinc.com/digilent-plugin-for-xilinx-tools-download-only/

Go to the ISE installation directory and create the following path.

    /<ISE_PATH>/ISE_DS/ISE/lib/lin64/plugins/Digilent/libCseDigilent

**lin64** for 64 bits OS. Use **lin** instead for 32 bits OS.

Copy the `libCseDigilent.so` and `libCseDigilent.xml` files into that path.

Go to the $HOME folder and create:

    /<$HOME>/.cse/lin64/14.4/plugins/Digilent/libCseDigilent

Copy the `libCseDigilent.so` and `libCseDigilent.xml` files inside again.

Download the `Digilent Adept 2` software and install it.

    http://store.digilentinc.com/digilent-adept-2-download-only/

Reboot your PC.

That's all.


Problems with wbgen2 y the lua libraries.
===================================

lua5.1 is compatible with `Ubuntu 14.04`. Therefore, to check your lua version;

  lua -v

To install other lua distribution;

  sudo apt-get remove `luaX.X*`

To install `lua5.1`:

  sudo apt-get install lua5.1 lua5.1-md5 lua5.1-bitop nmap 

  


