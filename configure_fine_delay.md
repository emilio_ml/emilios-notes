General notes to configure the Fine Delay and DIO drivers.
===

The older linux kernels (3.2.0-XX) don't support some wired ethernet interfaces and it's needed to install the specific driver. Take a look in this issue:

http://ubuntuforums.org/showthread.php?t=1505697

The ethernet linux kernel must be installed. The very specific OS which supports the FD drivers is the Ubuntu 12.04 (32 bits) (kernel 3.2.0-XX) and the fine-delay-sw-v2014-04 branch. If you have found somelse newer kernel which compiles and works within the Fine Delay, please let me know. I wasn't able :/

Instructions for Fine Delay board.
===

1- Install Ubuntu 12.04 (swap 4G,  / 30G and the rest is for /home/ ).
--------------------

Update the OS; the last linux kernel available is 3.2.0-83.

2- Download and compile the FD repository and and the DIO compatible module:
--------------------

Download the repo

    git clone git://ohwr.org/fmc-projects/fmc-delay-1ns-8cha/fine-delay-sw.git
    cd fine-delay-sw
    git checkout -b fine-delay-sw-v2014-04-hotfix fine-delay-sw-v2014-04-hotfix
    git submodule init
    git submodule update

Compile and install the repo. Note $CROSS_COMPILE must be empty and ensure that the fmc and spec modules are removed.

    make
    sudo make prereq_install
    sudo make install gateware_install
    sudo make install

To use the spec.ko you need the golden spec-init.bin at the /lib/firmware/fmc/, as well as wr_nic_dio.bin for the wr-nic.ko

    wget http://www.ohwr.org/attachments/download/2744/wr-starting-kit-v2.0_gw.tar.gz
    tar -zxvf wr-starting-kit-v2.0_gw.tar.gz spec-init_R057.bin wr_nic_dio-v2.0.bin
    sudo mv spec-init_R057.bin /lib/firmware/fmc/spec-init.bin
    sudo mv wr_nic_dio-v2.0.bin /lib/firmware/fmc/wr_nic_dio.bin
    rm wr-starting-kit-v2.0_gw.tar.gz

The spec-fine-delay-v2.0-20140331.bin file installed at /lib/firmware/fmc/ folder doesn't work properly. There are several messages *warning timestamps are not available* at the wrc# shell. I don't know where is coming from this issue but it's fixed by spec-fine-delay-v2.1-20141209.bin. Therefore, the commands to download & install it are:

    wget http://www.ohwr.org/attachments/download/3713/spec-fine-delay-v2.1-20141209.bin
    sudo rm /lib/firmware/fmc/spec-fine-delay.bin
    sudo mv spec-fine-delay-v2.1-20141209.bin /lib/firmware/fmc/spec-fine-delay-v2.1-20141209.bin
    sudo ln -s /lib/firmware/fmc/spec-fine-delay-v2.1-20141209.bin /lib/firmware/fmc/spec-fine-delay.bin
    rm spec-fine-delay-v2.1-20141209.bin

Install the wr-nic.ko kernel module.

    cd .../fine-delay-sw/spec-sw
    make
    sudo mv kernel/wr-nic.ko /lib/modules/`uname -r`/extra/wr-nic.ko

You should add the `extra` to the search of module so that at next reboot kernel modules are easily found.

Open the configuration file

    sudo gedit /etc/depmod.d/ubuntu.conf

Check the `extra` keyword is before `built-in`

    search updates ubuntu extra built-in

3- Install the drivers.
--------------------

Update & insert the drivers. The dependencies order is fmc->spec->zio->fmc-finde-delay.

    sudo depmod -a
    sudo modprobe fmc
    sudo modprobe spec
    dmesg
    sudo modprobe zio
    sudo modprobe fmc-fine-delay
    dmesg
    sudo modprobe wr-nic
    dmesg

Some useful module commands.

    modinfo module
    lsmod | grep module
    sudo modprobe module
    sudo modprobe -r module
    sudo insmod module
    sudo rmmod module

Now you are ready to use the the files at the `tool/` folder to play with the Fine Delay board.


Boot scripts to load the drivers and set some specific config into the boards
===

To set automatically some configuration when the system is booting up, just write a bash script and point to it from rc.local.

For instance, this script loads the wr-nic and fmc-fine-delay drivers and configures the fine-delay to ouput 10MHz and PPS signals.

    # #!/bin/bash
    date > /tmp/wrboot_date

    # Loading the Fine Delay driver #
    echo `modprobe fmc-fine-delay`

    # Loading the WR-NIC (DIO) driver
    echo `modprobe wr-nic`

    # Enabling WR on the FD
    echo `fmc-fdelay-board-time wr` #

    # Enabling the PPS and 10 MHz output on the FD ( 1 & 2 channels)
    # PPS
    echo `fmc-fdelay-pulse -o 1 -p` #
    # 10 MHz
    echo `fmc-fdelay-pulse -o 2 -1` #

... and edit the */etc/rc.local* file:

    #!/bin/sh -e

    sudo /home/seven/Desktop/wr/wr_boot_script.sh > /tmp/wrboot

    exit 0

The output logs are stored at /tmp/ folder.

Enjoy!

Remarks.
===

Note that the fmc.ko and spec.ko will be automatically loaded every time you restart the workstation.

What I know about the Fine Delay issues.
===

1.
--------------------

There is no problem to compile and install the latest fine delay tag (*fine-delay-sw-v2015-02*) with Ubuntu 12.04, 14.04 & 14.10 (3.2.0, 3.13 & 3.16 kernels) but when the fmc-fine-delay.ko is loaded this message shows up:

    [  533.146902] spec 0000:05:00.0: reprogramming with fmc/spec-fine-delay.bin
    [  533.340397] spec 0000:05:00.0: FPGA programming successful
    [  533.340975] fmc_fine_delay FmcDelay1ns4cha-0500: Gateware successfully loaded
    [  533.340985] kernel tried to execute NX-protected page - exploit attempt? (uid: 0)
    [  533.340986] BUG: unable to handle kernel paging request at ffff880101ff1ce8
    [  533.340988] IP: [<ffff880101ff1ce8>] 0xffff880101ff1ce7
    [  533.340992] PGD 1c06063 PUD d8b96067 PMD 101eaf063 PTE 8000000101ff1163
    [  533.340994] Oops: 0011 [#1] SMP
    [  533.340996] CPU 0
    [  533.340997] Modules linked in: fmc_fine_delay(O+) zio(O) spec(O) fmc(O) vesafb snd_hda_codec_realtek rfcomm bnep bluetooth cp210x joydev usbserial usbhid hid mxm_wmi ppdev eeepc_wmi asus_wmi sparse_keymap snd_hda_intel snd_hda_codec snd_hwdep snd_pcm snd_seq_midi snd_rawmidi snd_seq_midi_event parport_pc snd_seq video snd_timer snd_seq_device e1000e(O) acpi_pad psmouse mac_hid serio_raw snd wmi soundcore snd_page_alloc lp shpchp parport usb_storage [last unloaded: zio]
    [  533.341012]

    [...]

    [  533.341077]  [<ffffffffa01e5782>] fd_init+0x22/0x40 [fmc_fine_delay]
    [  533.341080]  [<ffffffff81002040>] do_one_initcall+0x40/0x180
    [  533.341083]  [<ffffffff810aa5ae>] sys_init_module+0xbe/0x230
    [  533.341085]  [<ffffffff8166d402>] system_call_fastpath+0x16/0x1b
    [  533.341086] Code: 88 ff ff 8e 87 1e a0 ff ff ff ff 34 1d ff 01 01 88 ff ff 42 ce 00 00 00 00 00 00 ae 9c 1e a0 ff ff ff ff ff ff ff ff 1a de 9e f1 <49> bb 10 86 1e a0 ff ff ff ff 49 ba c8 1c ff 01 01 88 ff ff 49
    [  533.341104] RIP  [<ffff880101ff1ce8>] 0xffff880101ff1ce7
    [  533.341106]  RSP <ffff880101ff1c80>
    [  533.341106] CR2: ffff880101ff1ce8
    [  533.341108] ---[ end trace 133bd715bb910c78 ]---

2.
--------------------

The tag fine-delay-sw-v2014-04-hotfix doesn't compile for 3.13 & 3.16 kernel but it's OK for 3.2.0-XX (Ubuntu 12.04).

3.
--------------------

If fmc-fine-delay.ko is loaded->unloaded->loaded several times, it could freeze up the workstation.

4.
--------------------

The only one configuration which works, that I have been able to find out, is:

    Ubuntu 12.04 (3.2.0-XX kernel) and the tag fine-delay-sw-v2014-04-hotfix

Moreover, the list of MOBOs which work along with the FD:

      - P8Z77-M PRO (Rafa & HDL_SERVER)
      - Z97M-G43(MS-7924) (INRIM)
      - P7P55 WS SUPERCOMPUTER  (EMILIO)

... and the troublesome MOBOs list:

      - H97M-PLUS
      - Fujitsu D3227 (DLR)
