General notes to configure the starting kit and play with the DIO card.
===================================

*Note the fmc.ko and spec.ko from this repo don't work within the Fine-Delay*

The best way to proceed is downloading and installing the wr-starting-kit repo.

    git clone git://ohwr.org/white-rabbit/wr-starting-kit.git
    cd wr-starting-kit
    git checkout -b wr-starting-kit-v2.0 wr-starting-kit-v2.0

    git submodule init
    git submodule update

Update submodule of spec-sw

    cd spec-sw
    git submodule init
    git submodule update
    cd ..

Compile and install the repo

    make
    sudo make install

You should add the `extra` to the search of module so that at next reboot kernel modules are easily found.


Open the configuration file

    sudo gedit /etc/depmod.d/ubuntu.conf

Check the `extra` keyword is before `built-in`

    search updates ubuntu extra built-in


Generate the map dependencies.

    sudo depmap -a

Install the firmware

    sudo scripts/wr-ssk-get --all


Load the drivers. The dependencies order is fmc->spec->wr-nic.

Update & isert the drivers. The dependencies order is fmc->spec->zio->fmc-finde-delay.

    sudo depmod -a
    sudo modprobe fmc
    sudo modprobe spec
    dmesg
    sudo modprobe wr-nic
    dmesg

Now you are able to play with the DIO channels through the files placed at `spec-sw/tools/`.

____

Notes:
--------------------

The following configuration compiles but it doesn't work:

		The Ubuntu 14.04 (kernel 4.4.0-07-generic) with spec-sw commit 5f15d26e0e90a0e22f520a7185a940003bac799c. At the same time the wr-starting-kit uses etherbone core and its commit c1e676dc9d35028910c50431d70328e522396c89.

The good one must be some kernel between the 2.8.x and the 3.5.x versions over the tag wr-starting-kit-v2.0. If you have a higher kernel version you can do the downgrade throught this blog:

http://www.yourownlinux.com/2014/08/how-to-install-upgrade-to-linux-kernel-3-15-10-in-linux.html





