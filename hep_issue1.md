h1. HEP 1: Remove the red LED from the system and debug the stability problem.

h2. Description.

The last wrc-2p-sw used for the HEP WR-TP (Mattia GM) is not able to complete the external servo FSM so we must debug it because the red error LED is on.

Also it's a good oportunity to debug the stability problem apparently introduced by this commit:

https://bitbucket.org/sevensols/wrc-2p-sw/commits/24bbd6a0a934353a739cb37d022acbc7658128c7

The Phase Noise Analyzer is needed  for this last purpose.

h2. Resources.

We must start from this branch:

https://bitbucket.org/sevensols/wrc-2p-sw/commits/branch/master_wrz

Don't forget to remove this commit:

https://bitbucket.org/sevensols/wrc-2p-sw/commits/24bbd6a0a934353a739cb37d022acbc7658128c7

and set the PI parameters to:

{code:java}
#define MAIN_SPLL_KP 600;
#define MAIN_SPLL_KI 2;	
{code}

h2. Where is it supposed to work:

WR-TP used by HEP. It has enabled the GM Mattia mode in GW.

h2. Dependencies:

Apparently none.

h2. Is it necessary test?:

Two issues two test:

h4. 1) Removing the red LED: 

1.1)The timing team must check that the GM mode works, the red LED is vanished and the external servo FSM is able to finished its sequence.

1.2) The HEP team must check that the GM mode works without the red LED and try several times in several conditions.

h4. 2) Removing the stability problem when the mentioned commit is present.

2.1) The timing team must find the reason and debug it. Also it must check by using the Phase Noise Analyzer that the stability requs. are met.

2.2) The HEP team must double check that the stability requs. are met.
