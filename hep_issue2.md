h1. HEP 2: Removing the need of external PPS in the GM mode.

h2. Description.

Starting from this closed issue.

https://sevensols.ovh:8443/browse/FW-857

HEP department needs a GM mode which doesn't use the external PPS and it produces its own internal PPS. The idea is to use the external clock reference (10MHz or 62.5MHz) to generate a PPS by a simple counting process.

*Important:* To produce the new PPS, use the external clock falling edges to avoid this efect :

https://www.ohwr.org/project/white-rabbit/uploads/ae3282acd8f9f6c5a9067b061202277d/wr_external_reference.pdf

The new PPS must be routed to the WR-CORE inside the WR-TP as external PPS.

That's all!

h2. Resources.

The WR-TP FPGA gateware. This branch is the starting point.

https://bitbucket.org/sevensols/wr-zen-hdl/commits/branch/ext-ad9516-gm

h2. Where is it supposed to work?

Right now just the WR-TP, *not the TP-FL*.

Ask HEP in case of doubt.

h2. Dependencies.

The developer must borrow a WR-TP to someone.

h2. Is it necessary test?

Yes, a functional test is needed by the timing team and also by the HEP team.

A) The timing team must check that the WR-TP is able to work in GM mode without the external PPS or any problem by checking the red LED and the external servo FMS.

B) The HEP team must check that the WR-TP is able to work in GM mode without the external PPS and test it serveral times.
